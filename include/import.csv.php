<?php

require_once "import.del.php";

class ImportCsv extends ImportDel {

  function ImportCsv( $file_lines
                    , $delims = array(";", ",", "\t")
                    , $quotes = array('"', "'")
                    ) {
                    	
    function maxChar($chars, $testString) {
    	
    	$max_count = 0;		
      $the_char  = (count($chars) > 0 ? $chars[0] : " ");
      foreach($chars as $char) {
      	$new_count = substr_count($testString, $char);
      	if($new_count > $max_count) {
      		$max_count = $new_count;
      		$the_char  = $char;
      	}
      }
      
      return $the_char;
    }
    
    $test_line = $file_lines[0];
    
    //
    // Detect the most probable delimiter.
    //
    $delim = maxChar($delims, $test_line);
    $quote = maxChar($quotes, $test_line);

    $escape = '\\';

    for($i = 0; $i < count($file_lines); $i++) {
        // Skip the first line (header)
        if ($i > 0)
        {
            $row = str_getcsv($file_lines[$i], $delim, $quote, $escape);
            $this->data[] = $row;
        }
    }

    //
    // Convert the array to addresses
    //
    $this->convertToAddresses(); // Will populate ab
  }
}
?>
